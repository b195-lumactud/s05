package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService{
    @Autowired//injects code from classes files that we have imported
    private UserRepository userRepository;


    public void createUser (User user){
        userRepository.save(user);
    }


    public ResponseEntity updateUser(Long id, User user) {
        User userForUpdating = userRepository.findById(id).get();
        userForUpdating.setUsername(user.getUsername());
        userForUpdating.setPassword(user.getPassword());

        if (id.equals(userForUpdating)){
            userRepository.save(user);
            return new ResponseEntity<>("Username updated successfully", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Credentials not existing", HttpStatus.UNAUTHORIZED);
        }
    }


    public ResponseEntity deleteUser(Long id) {
        User userForDeleting = userRepository.findById(id).get();


        if (id.equals(userForDeleting)){
            userRepository.deleteById(userForDeleting);
            return new ResponseEntity<>("Username updated successfully", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Credentials not existing", HttpStatus.UNAUTHORIZED);
        }
    }

    public Optional<User> findByUsername(String Username){
        return Optional.ofNullable(userRepository.findByUsername(Username));
    }

}
