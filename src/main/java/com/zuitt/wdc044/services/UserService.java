package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.models.User;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface UserService {
    void createUser(User user);

    ResponseEntity updateUser(Long id, String username, String password);

    ResponseEntity deleteUser(Long id);

    Optional<User> findByUsername(String username);
}
